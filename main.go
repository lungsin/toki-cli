// More info on Getwd()
// https://golang.org/src/os/getwd.go
// 
package main
import(
    "os" 
    "os/exec"
	"fmt"
    "path"
    "bytes"
    "strings"
)

func getRootFromGit() string {
    cmd := exec.Command("git", "rev-parse", "--show-toplevel")
    var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		panic(err)
    }
    return out.String()
}

func getRootFromPath(dir string) string {
    return path.Dir(path.Dir(dir))
}

func checkRoot(dir string) {
    root := getRootFromGit()
    if strings.Trim(root, "\n") != strings.Trim(getRootFromPath(dir), "\n") {
        fmt.Println(root, getRootFromPath(dir))
        
        panic("Current directory must be a level 2 directory")
    }
}

func pwd() string {
    dir, err := os.Getwd()
    if err != nil {
        panic(err)
    }
    return dir
}

func initComponents() map[string][]string {
    components := map[string][]string {
        "description" : []string{"statement.txt", "render"}, 
        "solution" : []string{"solution.cpp"},
        "runner" : []string{"spec.cpp"},
        "scorer" : []string{"scorer.cpp"},
        "alt-solution" : []string{"alt-solution.cpp"},
        "communicator" : []string{"communicator.cpp"},
    }
    // solution-x
    for i:= 1; i < 10; i++ {
        name    := fmt.Sprintf("solution-%d", i)
        fname   := fmt.Sprintf("%s.cpp", name)
        components[name] = []string{fname}
    }
    
    return components
}

func gitAdd(file string) {
    dir := pwd()
    cmd := exec.Command("git", "add", dir + "/" + file)
	cmd.Run()
}

func gitCommit(slug, component string) (isChange bool) {
    fmt.Println("Commiting ", slug, component)
    commitMsg := fmt.Sprintf("'Add %s'", component)
    err := exec.Command("git", "commit", "-m", commitMsg).Run()
    isChange = (err == nil)
    return
}

func gitCreateBranch(branchName string){
    exec.Command("git", "checkout", "master").Run()
    err := exec.Command("git", "checkout", "-b", branchName).Run()
    if err != nil {
        exec.Command("git", "checkout", branchName).Run()
    }
}

// exists returns whether the given file or directory exists
func exists(path string) (bool, error) {
    _, err := os.Stat(path)
    if err == nil { return true, nil }
    if os.IsNotExist(err) { return false, nil }
    return true, err
}

func commit(slug, component string, files []string) bool {
    exist := false
    for _, file:= range files {
        path := fmt.Sprintf("%s/%s", pwd(), file)
        if ok, _ := exists(path); ok {
            exist = true    
        }
    }
    if !exist {
        return false
    }
    branchName := fmt.Sprintf("%s_%s", slug, component)
    gitCreateBranch(branchName)
    for _, file := range files {
        gitAdd(file)
    }
    isChange := gitCommit(slug, component)
    
    if isChange {
        fmt.Println("git push origin")
        exec.Command("git", "push", "origin", branchName).Run()
    }
    return isChange
}

func createMr(slug, component, label string) {
    
    message := fmt.Sprintf("%s %s", slug, component)
    fmt.Printf("Creating Mr for %s\n", message)
    err := exec.Command("lab", "mr", "create", "-l", component, "-l", label, "-m",  message).Run()
    if (err != nil) {
        fmt.Println(err)
    } else {
        fmt.Println("create mr success")
    }
}

func main() {
    dir := pwd()
    checkRoot(dir)
    slug := path.Base(dir)
    components := initComponents()
    
    for component, files := range components {
        if commit(slug, component, files) {
            // get directory one level above
            label := path.Base(path.Dir(dir)) 
            createMr(slug, component, label)
        }
    }
    
    exec.Command("git", "checkout", "master").Run()
}