# toki-cli


## Installation

1. Install golang

2. Install [lab](https://github.com/zaquestion/lab) cli

## Walkthrough

1. Preparation: di branch master, bikin component soal di directory `<kategori>/<slug soal>`. Misal: `latihan-1/aliens`

2. Pastikan sekarang ada di directory `<kategori>/<slug soal>`

3. Jalankan script `go run main.go`

4. Yang akan dilakukan script:
    
    - membuat branch untuk masing2 component yang dibuat. Nama branch nya adalah `<slug soal>_<component>`. 
    
        Contoh: apabila di directory `latihan-1/aliens` ada `statement.txt`, `solution.cpp` dan `spec.cpp`; maka akan dibuat branch `aliens_description`, `aliens_solution`, dan `aliens_runner`. 
    
    - push branch2 tsb ke remote.
    
    - membuat merge request dengan judul `<slug> <component>` dengan label `<kategori>` dan `<component>`. 
    
        Contoh: apabila sekarang berada di directory `latihan-1/aliens`, dan branch skrg adalah `aliens_solution` maka akan dibuat merge request dengan judul `aliens solution` dengan label `latihan-1` dan `solution`. 
    

